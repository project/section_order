
The Section Order module exists becuse I got tired of having to either
create hooks to let CCK know how to reorder $content (theme variable), or
$node->content['section name'], or adding settings to my admin forms to
control the section weights.

This module watches as the content is viewed and keeps track of the
actual sections that are added to the content.

You, the site administrator, are then able to use a simple drag-and-drop
form to re-order these sections in any way you wish, regardless of the
weights the other module creator specified. This gives you quite a bit of
flexibility. Such a feature has been asked for from modules such as Image
Attach and Taxonomy Image. Now it exists.

Okay, so now a lot of people are asking, "Why not just have CCK do it?" There
are several potential problems with that scenario:
1) Sacrelgious though may sound, there are sites that don't have CCK installed.
2) It seems that most contributed modules do not bother telling CCK about
   about their content sections, so they cannot be reordered with CCK.
3) Even when those developers do tell CCK, occasionally they do it incorrectly,
   again leaving CCK without the correct capability.
4) CCK only has one ordering capability, so teasers and full nodes look
   the same.

SO keeps different information about the sections in teaser and full-node
displays. It allows you to specify a different order for each. So, for example
you can specify that an image appears above of the body in teaser view, and
below the body in full-node view.

The Section Order module works with or without CCK. Since it runs at the highest
module weight, it will always be the last module to look at the content. This
means that it will not get stomped on by other modules. If CCK is running, SO
will simply alter CCK's weight values or provide section weights if CCK doesn't
already know about them. If CCK is not available, then SO will alter the section
weight directly. Either way, you get the desired order.

Since SO wants to be the last module to run, the enable function sets its module
weight higher than the highest weight in the database. If you enable new modules
that may run at a higher weight, then you may simply disable the SO module
(but do not uninstall it) and then re-enable it. That will set the SO module
back to the new highest weight.
